package com.jcg.jpa.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class JPADemo extends HttpServlet {
	
private static final EntityManagerFactory emFactoryObj;
private static final String PERSISTENCE_UNIT_NAME = "JPADemo";  

static {
	emFactoryObj = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME); 
}

public static EntityManager getEntityManager() {
	return emFactoryObj.createEntityManager();
}

public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	EntityManager entityMgr = getEntityManager();
	entityMgr.getTransaction().begin();
	
	Farmer farmObj = new Farmer();
	farmObj.setId(105);
	farmObj.setName("Harry Potter");
	farmObj.setVillage("Scotish Highlands");
	
	entityMgr.persist(farmObj);
	entityMgr.getTransaction().commit();
	entityMgr.clear();
	PrintWriter out = response.getWriter();
	out.println("<html> " + "<body>" + "<h1>Hi</h1>" + "</body>" + "</html>");
}



}
